from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
import os
from sample_queries import SampleQueries
from pydantic import BaseModel

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "year": 2022,
            "month": 12,
            "day": "9",
            "hour": 19,
            "min": 0,
            "tz:": "PST"
        }
    }


@app.get("/api/samples")
def get_samples(
    queries: SampleQueries = Depends(),
):
    return queries.get_samples()


class SampleIn(BaseModel):
    required_limited_text: str
    required_unlimited_text: str
    required_integer: int


@app.post("/api/samples")
def create_sample(
    sample: SampleIn,
    queries: SampleQueries = Depends(),
):
    return queries.create_sample(sample)
