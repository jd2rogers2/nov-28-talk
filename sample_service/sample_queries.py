import os
from psycopg_pool import ConnectionPool

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class SampleQueries:
    def get_samples(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT
                        *
                    FROM dummy
                    """
                )

                rows = cur.fetchall()
                return rows

    def create_sample(self, sample):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    f"""
                    INSERT INTO dummy (
                        required_limited_text,
                        required_unlimited_text,
                        required_integer
                    ) VALUES (
                        '{sample.required_limited_text}',
                        '{sample.required_unlimited_text}',
                        {sample.required_integer}
                    );
                    """
                )

                return True
